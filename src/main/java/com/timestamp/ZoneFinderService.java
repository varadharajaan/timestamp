package com.timestamp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 * @Author vardamod on 04/07/20 20:31
 * @Projectname timestamp
 */
@Service
public class ZoneFinderService {

    @Value("${availbility}")
    private  String availbilityZone;

    public String getAvailbilityZone() {

        try {
            RestTemplate restTemplate = new RestTemplate();

            String url = "http://169.254.169.254/latest/meta-data/placement/availability-zone";

            return restTemplate.getForObject(url, String.class);
        } catch (Exception e) {

            return availbilityZone;
        }
    }
}
