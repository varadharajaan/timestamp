package com.timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * @Author vardamod on 04/07/20 20:30
 * @Projectname timestamp
 */
@RestController
public class TimeStampController {


    ZoneFinderService zoneFinderService;

    @Autowired
    public void setZoneFinderService(ZoneFinderService zoneFinderService) {
        this.zoneFinderService = zoneFinderService;
    }

    @GetMapping("/time")
    public ResponseEntity<String> getTimeStampWithAZ () {

        Instant instant = Instant.now();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.of("Europe/Paris"));

        String  response = "Europe Time: ".concat(localDateTime.toString()).concat("   ").concat(" from   ").concat(zoneFinderService.getAvailbilityZone());

        return new ResponseEntity<>(response, HttpStatus.OK);

    }
}
